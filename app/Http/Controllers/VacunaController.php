<?php

namespace App\Http\Controllers;

use App\Models\Grupo;
use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class VacunaController extends Controller
{
    public function index()
    {
        $vacunas=Vacuna::all();
        return view("vacunas.index",compact("vacunas"));
    }

    public function show(Vacuna $vacuna)
    {
        return view("vacunas.show",compact("vacuna"));
    }

    public function buscador(){
        return view("pacientes.buscador");
    }

    /**
     * @param Request $request
     * @return boolean
     */
    public function store(Request $request){

        $vacuna=new Vacuna();
        $vacuna->especie=$request->especie;
        $vacuna->slug=Str::slug($request->especie);

        $vacuna->save();

        return response()->json(['mensaje'=>"Vacuna {$vacuna->nombre} INSERTADA"]);

    }

    /**
     * @param integer $id_paciente
     * @return boolean
     */
    public function mostrar($id_paciente){
        $paciente=Paciente::where("id",$id_paciente)->get();
        $grupos=Grupo::where("id",$paciente->grupo_id)->get();
        $vacunas=Vacuna::where("")->get();

        return response()->json($grupos);
    }




}
