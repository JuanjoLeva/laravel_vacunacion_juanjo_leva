@extends("layouts.master")
@section("titulo")
    Titulo
@endsection

@section("contenido")
        <div class="container">
            <div class="row">

                @foreach( $vacunas as $vacuna )
                    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12" style="margin:30px 10px 0">
                        <div class="card-body w-50  mx-auto d-block">
                            <h4 style="min-height:45px;margin:5px 0 10px 0">
                                <a href="{{ route('vacunas.show',$vacuna) }}">
                                    {{$vacuna->nombre}}
                                </a>
                            </h4>
                            <h6 style="min-height:45px;margin:5px 0 10px 0">
                                Posibles grupos de vacunacion:
                                @foreach($vacuna->grupos as $grupos)
                                    <li>{{$grupos->nombre}}</li>
                                @endforeach
                            </h6>
                            <br>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
@endsection
