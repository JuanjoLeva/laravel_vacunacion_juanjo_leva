@extends("layouts.master")

@section("titulo")
    show
@endsection

@section("contenido")
    <div class="container">
        @if(Session::has('mensaje'))

            {{Session::get('mensaje')}}
        @endif
        <div class="row">
            <div class="col-sm-3">

            </div>

            <div class="col-sm-9">
                <h3 class="align-center" style="min-height:45px;margin:5px 0 10px 0" >
                    {{$vacuna->nombre}}
                </h3>
                <h3 class="align-center" style="min-height:45px;margin:5px 0 10px 0" >
                    Pacientes NO vacunados:
                </h3>
                @foreach($vacuna->grupos as $grupos)
                    @foreach($grupos->pacientes as $paciente)
                        @if($paciente->vacunado == false)
                            {{$paciente->nombre}} -- {{$grupos->nombre}} --{{$grupos->prioridad}}-- <strong>NO VACUNADO</strong>
                            <a href="{{ route('paciente.vacunar', $paciente) }}"><button class="btn btn-danger">Vacunar</button></a>
                            <br><br>
                        @endif
                    @endforeach
                @endforeach
                <br><br>
                <h3 class="align-center" style="min-height:45px;margin:5px 0 10px 0" >
                    Pacientes vacunados:
                </h3>
                @foreach($vacuna->grupos as $grupos)
                    <strong>{{$grupos->nombre}}</strong><br>
                    @foreach($grupos->pacientes as $paciente)
                        @if($paciente->vacunado == true)
                            {{$paciente->nombre}} -- {{$grupos->nombre}} --{{$paciente->fechaVacuna}}-- <strong>VACUNADO</strong><br>
                        @endif
                    @endforeach
                    <br>
                @endforeach

            </div>
        </div>
    </div>
@endsection
