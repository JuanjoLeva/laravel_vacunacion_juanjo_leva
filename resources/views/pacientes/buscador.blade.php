@extends("layouts.master")

@section("titulo")
    create
@endsection

@section("contenido")
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
        </div>
    </div>
@endsection

<script>

    $(document).ready(function(){

        $("#busqueda").autocomplete({
            source: function( request, response ) {
                $.ajax( {
                    type: "POST",
                    url: "{{url('pacientes/buscar')}}",
                    dataType: "json",
                    data: {"_token": "{{csrf_token()}}",
                        "busqueda": request.term
                    },
                    success: function( data ) {
                        response( data );
                    }
                } );
            },
            position:{
                my: "left+0 top+8 ",
            },
            select:function (event,ui){
                window.location=window.location.origin + "/pacientes/" + convertToSlug(ui.item.value);
            }
        } );
        function convertToSlug(Text){
            return Text
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
        }
    });
</script>
