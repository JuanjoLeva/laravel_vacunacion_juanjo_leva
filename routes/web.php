<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\VacunaController::class,"index"])->name("vacunas.index");

Route::get('vacunas', [\App\Http\Controllers\VacunaController::class,"index"])->name("vacunas.index");

Route::get('vacunas/{vacuna}', [\App\Http\Controllers\VacunaController::class,"show"])->name("vacunas.show");

Route::get('pacientes/{paciente}/vacunar', [\App\Http\Controllers\PacienteController::class,"vacunar"])->name("paciente.vacunar");

Route::get('pacientes/buscador', [\App\Http\Controllers\VacunaController::class,"buscador"])->name("paciente.buscador");

Route::post('pacientes/buscar', [\App\Http\Controllers\PacienteController::class,'buscar'])->name("pacientes.buscar");

Route::post('api/vacunas/crear',[\App\Http\Controllers\VacunaController::class,'store'])->name("vacunas.crear");

Route::get('api/vacunas/{paciente}',[\App\Http\Controllers\VacunaController::class,'mostrar'])->name("vacunas.mostrar");
