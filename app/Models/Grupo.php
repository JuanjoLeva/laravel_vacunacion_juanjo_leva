<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function pacientes()
    {
        return $this->hasMany(Paciente::class);
    }

    public function vacunas()
    {
        return $this->belongsToMany(Vacuna::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
