<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PacienteController extends Controller
{
    public function vacunar(Paciente $paciente)
    {
        DB::table("pacientes")->where("id",'=',$paciente->id)->update(array('vacunado' => true));
        DB::table("pacientes")->where("id",'=',$paciente->id)->update(array('fechaVacuna' => date(now())));

        return redirect()->back()->with('mensaje','SE HA VACUNADO A '. $paciente->nombre);
    }
    public function buscar(Request $request){
        $busqueda=Paciente::select("nombre")->where("nombre","like","%".$request->busqueda."%")->pluck("nombre");

        return response()->json($busqueda);
    }
}
